# Jood

![Elegant Objects Respected Here](http://www.elegantobjects.org/badge.svg)

![nullfree status](https://iwillfailyou.com/nullfree/nikialeksey/jood)

[![Lib version](https://img.shields.io/maven-central/v/com.nikialeksey/jood.svg?label=lib)](https://maven-badges.herokuapp.com/maven-central/com.nikialeksey/jood)
[![Build Status](https://travis-ci.org/nikialeksey/jood.svg?branch=master)](https://travis-ci.org/nikialeksey/jood)
[![codecov](https://codecov.io/gh/nikialeksey/jood/branch/master/graph/badge.svg)](https://codecov.io/gh/nikialeksey/jood)

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://github.com/nikialeksey/jood/blob/master/LICENSE)

Java object oriented sql-database library
